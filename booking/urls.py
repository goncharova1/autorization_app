from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from api.views import GoogleLogin, index
from social_django import views as social_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('api/auth/login/google-oauth2/', social_views.auth, {'backend': 'google-oauth2'}, name='begin'),
    path('api-auth/', include('rest_framework.urls')),
    path('api-token-auth/', obtain_auth_token),
    path('api/auth/google/', GoogleLogin.as_view(), name='google_login'),
    path('api/auth/', include('social_django.urls', namespace='social')),
]
